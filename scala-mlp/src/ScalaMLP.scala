
import java.util
import java.util.ArrayList;

/**
  * Created by kevin on 2/2/16.
  */
object ScalaMLP {

  def main(args: Array[String]) {
    if (false) {
      println("Firing test")

      println("Create nn")
      val inp = List(0.05, 0.1)
      val targets = List(0.01, 0.99)

      println("Create hidden neurons")

      val h1 = new HiddenNeuron(List(0.15, 0.2), 0.35, logisticSigmoid, logisticSigmoidPrime)
      val h2 = new HiddenNeuron(List(0.25, 0.3), 0.35, logisticSigmoid, logisticSigmoidPrime)
      println("Create hidden layer")
      val hl = new HiddenLayer(List(h1, h2))

      println("DEBUG: h2 out" + h1.fire(inp))
      println("DEBUG: h1 out" + h2.fire(inp))

      println("Create output neurons")
      val o1 = new OutputNeuron(List(0.40, 0.45), 0.60, logisticSigmoid, logisticSigmoidPrime)
      val o2 = new OutputNeuron(List(0.50, 0.55), 0.60, logisticSigmoid, logisticSigmoidPrime)
      println("Create output layer")
      val ol = new OutputLayer(List(o1, o2))

      println("Create nn")
      val nn = new MLP(List(hl, ol))

      println("output: " + nn.fire(inp))
    } else {

      val h1 = new HiddenNeuron(List(0.15, 0.2), 0.35, logisticSigmoid, logisticSigmoidPrime)
      val h2 = new HiddenNeuron(List(0.25, 0.3), 0.35, logisticSigmoid, logisticSigmoidPrime)
      val hl = new HiddenLayer(List(h1, h2))

      val o1 = new OutputNeuron(List(0.40, 0.45), 0.60, logisticSigmoid, logisticSigmoidPrime)
      val o2 = new OutputNeuron(List(0.50, 0.55), 0.60, logisticSigmoid, logisticSigmoidPrime)
      val ol = new OutputLayer(List(o1, o2))

      val nn = new MLP(List(hl, ol))

      val td1:TestData = new TestData(List(0.0, 0.0), List(0.0))
      val td2:TestData = new TestData(List(0.0, 1.0), List(1.0))
      val td3:TestData = new TestData(List(1.0, 0.0), List(1.0))
      val td4:TestData = new TestData(List(1.0, 1.0), List(0.0))

      val trainConf:TrainConf = new TrainConf(0.75, 20, 100, 0.1)

      if(nn.train(List(td1, td2, td3, td4), trainConf))
        println("Training XOR: SUCCESS")
      else
        println("Training XOR: FAILURE")

    }

  }

  case class TestData(inp:List[Double], out:List[Double])
  case class TrainConf(alpha:Double, sets:Int, loops:Int, goal:Double)
  class Delta()

  //flags
  val out: String = "Output"
  val hid: String = "Hidden"

  //functions
  def logisticSigmoid(x:Double):Double={1 / (1.0 + Math.exp(-x))}
  def logisticSigmoidPrime(x:Double):Double={x * (1.0 - x)}

  trait Neuron{
    val weightList:List[Double]
    val bias:Double
    def actFn(x:Double):Double
    def actPrimeFn(x:Double):Double
    var output:Double = 0.0
    var input:List[Double] = List(0.0)
    var delta:Double = 0.0

    def adjWeights(inputs: List[Double]=input, delta: Double=delta, alpha: Double): Neuron = {
      this match {
        case n:OutputNeuron =>
          new OutputNeuron(
            (weightList, inputs).zipped.map((w, i) => w + (delta * i - alpha)), //update weight list
            bias + (delta * 1 * -alpha),
            actFn,
            actPrimeFn)
        case n:HiddenNeuron =>
          new HiddenNeuron(
            (weightList, inputs).zipped.map((w, i) => w + (delta * i - alpha)), //update weight list
            bias + (delta * 1 * -alpha),
            actFn,
            actPrimeFn)
      }
    }


    def fire(inp: List[Double]): Double = {
      input = inp
      output = actFn((inp zip weightList).foldLeft(bias)((acc:Double, x:Tuple2[Double, Double]) => acc + x._1 * x._2))
      output
    }
  }


  case class OutputNeuron(weightList: List[Double], bias: Double, actFnIn: (Double => Double), actPrimeFnIn: (Double => Double)) extends Neuron {
    def calcDelta(target:Double, output:Double): Double ={
      delta = (target - output) * output * (1 - output) * -1
      delta
    }
    def actFn(x:Double):Double ={actFnIn(x)}
    def actPrimeFn(x:Double):Double ={actPrimeFnIn(x)}
  }

  case class HiddenNeuron(weightList: List[Double], bias: Double, actFnIn: (Double => Double), actPrimeFnIn: (Double => Double)) extends Neuron {
    def calcDelta(deltaOut:List[Double], weightOut:List[Double], output:Double): Double = {
      delta = (output *
        (1.0 - output) *
          (deltaOut zip weightOut).map((x:Tuple2[Double, Double]) => x._1 * x._2).foldLeft(0.0)(_ + _))
      delta
    }
    def actFn(x:Double):Double ={actFnIn(x)}
    def actPrimeFn(x:Double):Double ={actPrimeFnIn(x)}
  }

  trait Layer {
    val n:List[Neuron]
    def fire(inp: List[Double]): List[Double]
    def adjWeights(alpha:Double):Layer = {
      this match{
        case l:HiddenLayer => new HiddenLayer(n.map(x=>x.adjWeights(alpha=alpha)).asInstanceOf[List[HiddenNeuron]])
        case l:OutputLayer => new OutputLayer(n.map(x=>x.adjWeights(alpha=alpha)).asInstanceOf[List[OutputNeuron]])
      }
    }
    var outputs:List[Double] = List()
    var deltas:List[Double] = List()
  }

  case class HiddenLayer(n: List[HiddenNeuron]) extends Layer {
    def fire(inp: List[Double]): List[Double] = {
      outputs = n.map(x => x.fire(inp))
      outputs
    }

    def calcDeltas(aboveDeltas:List[Double], aboveWeights:List[List[Double]], outputs:List[Double]): List[Double] = {
      deltas = ((n zip outputs) zip aboveWeights).map((x:Tuple2[(HiddenNeuron, Double), List[Double]]) => x._1._1.calcDelta(aboveDeltas, x._2, x._1._2))
      deltas
    }
  }
  case class OutputLayer(n:List[OutputNeuron]) extends Layer{
    /** Returns a list of outputs for the layer.*/
    def fire(inp:List[Double]):List[Double] = {
      outputs = n.map(x => x.fire(inp))
      outputs
    }

    /** Returns a list of deltas, one for each neuron in the layer.*/
    def calcDeltas(targets:List[Double], outputs:List[Double]=outputs):List[Double] = {
      deltas = ((n zip targets) zip outputs).map((x:Tuple2[(OutputNeuron, Double), Double]) => x._1._1.calcDelta(x._1._2, x._2))
      deltas
    }
  }
  class MLP(initLayers:List[Layer]){
    var allLayers:List[Layer] = initLayers

    def fire(inp:List[Double]):List[Double] ={
      allLayers.foldLeft(inp)((inputs:List[Double], x:Layer) => x.fire(inputs))
    }

    def calcDeltas(inp:List[Double]=null, testData:TestData, trainConf: TrainConf): List[List[Double]] = {
      //calc outputs for each layer
      if(inp != null)
        fire(inp)

      //calc deltas for each layer
      var prevLayer:Layer = null
      for(i <- allLayers.size to 0){
        allLayers(i - 1) match {
          case l:OutputLayer => l.calcDeltas(testData.out)
          case l:HiddenLayer => l.calcDeltas(prevLayer.deltas, prevLayer.n.map(x => x.weightList), l.outputs)
          case _ => println("Layer not of output or hidden type")
        }
        prevLayer = allLayers(i)
      }

      //return stored deltas
      allLayers.map(x => x.deltas)
    }

    def train(testData: List[TestData], trainConf: TrainConf): Boolean ={

      var errMap:Map[TestData, Double] = Map[TestData, Double]()
      for(i <- 0 to trainConf.loops){
        for(td <- testData){
          for(j <- 0 to trainConf.sets){
            fire(td.inp)
            calcDeltas(testData=td, trainConf=trainConf)
            //update layers
            allLayers = allLayers.map(x => x.adjWeights(trainConf.alpha))
            errMap = errMap + (td -> totalErr(allLayers.last.outputs, td.out))
          }
        }
        //check if can quit early
        //if(errMap.values.map(x => x < trainConf.goal).foldLeft(true)(_ && _))

      }
      errMap.values.map(x => x < trainConf.goal).foldLeft(true)(_ && _)
    }

    def err(out:Double, target:Double):Double = {
      Math.pow(target - out, 2)
    }

    def totalErr(out:List[Double], target:List[Double]):Double = {
      (out zip target).map(x => err(x._1, x._2)).foldLeft(0.0)(_ + _)
    }

  }

}
