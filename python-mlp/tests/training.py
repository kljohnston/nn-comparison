import sys
import os
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/../src")
from main import *


def training_test():
    # create basic nn (based off example at
    # https://mattmazur.com/2015/03/17/a-step-by-step-backpropagation-example/)
    nn = {'fire': logistic_sigmoid,
          'neurons': (({'bias': 0.35, 'weights': (0.15, 0.20)},
                       {'bias': 0.35, 'weights': (0.25, 0.30)}),
                      ({'bias': 0.60, 'weights': (0.40, 0.45)},
                       {'bias': 0.60, 'weights': (0.50, 0.55)})),
          'fire_prime': logistic_sigmoid_prime}
    inputs = [0.05, 0.10]
    nn_signals = feedforward(nn, inputs)
    goals = [0.01, 0.99]
    learning_params = {
        'cost': cost,
        'cost_prime': cost_prime,
        'learning_rate': 0.5}
    adjustments_and_error = backpropagation(
        nn, nn_signals, goals, learning_params)

    # get the updated nn
    new_nn = apply_adjustments(nn, adjustments_and_error['adjustments'])

    # show that the nn can converge with time
    training_data = {
        'inputs': [inputs],
        'goals': [goals],
        'max_error': 0.00001,
        'max_tries': 100000}
    results = train_set(nn, learning_params, training_data)
    return results[1]


if __name__ == "__main__":
    # execute only if run as a script
    training_result = training_test()
    print "Successful training?: " + str(training_result)
    exit(int(not training_result))
