from contracts import contract, new_contract, ContractsMeta, with_metaclass
import functools
import operator
import math
import random
import numpy
from functools import reduce

OFF, ERROR, WARN, DEBUG, TRACE, = range(5)
debugging = OFF


def debug(msg, debug_lvl=OFF):
    if debug_lvl <= debugging:
        print str(msg)

# Contracts, validator functions, and helper functions
#######################################################

# things that compose a neuron:
# weights to input neurons, signal in, signal out, bias
# things that compose a layer:
# a list of neurons
# things that compose a nn:
# a list of layers, the activation_fn used by it
# things above a nn:
# the error function, backpropagation, delta structure (just data --
# derived from objects)

# Validators


def is_true(x):
    return True if x else False


def is_false(x):
    return not is_true(x)


def all_true(l, fn=None):
    lis = map(fn, l) if fn else l
    return not filter(is_false, lis)


def is_signal_in(x):
    return (isinstance(x, float) or
            isinstance(x, int))


def is_signal_out(x):
    return ((isinstance(x, float) or
             isinstance(x, int)) and
            x <= 1.0 and
            x >= -1.0)


def is_signal(signal):
    return (is_signal_in(signal['signal_in']) and
            is_signal_out(signal['signal_out']))


def is_signals(xs):
    return all_true(xs, is_signal)


def is_delta(delta):
    return is_signal_in(delta)  # same constraints apply


def is_weight(weight):
    return is_signal_in(weight)  # same constraints apply


def is_bias(bias):
    return (
        isinstance(
            bias,
            float) or isinstance(
            bias,
            int)) and bias <= 1.0 and bias >= 0.0


def sigma(fn, *args):
    '''
    Takes a function and some number of lists of arguments to it.
    The first value from each list is applied to the function.
  '''
    # zip together an unknown number of lists
    zip_fn = reduce(
        lambda acc,
        x: functools.partial(
            acc,
            x),
        args,
        functools.partial(zip))

    total = 0
    for arglist in zip_fn():
        total += fn(*arglist)
    return total


def results(signals):
    return signals_out_to_signals_in(signals)


def is_neuron(neuron):
    '''
  Validates the shape of a neuron.
  A neuron should be a dict containing a 'bias' and 1 or more 'weights'
  '''
    # verify bias
    valid = valid and is_bias(neuron.get('bias', -1))

    # verify weights
    for w in neuron['weights']:
        valid = valid and is_weight(w)
    valid = valid and len(neuron['weights']) > 0

    return valid


def is_neuron_deltas(neuron_deltas):
    '''
    Validates the shape of a neurons deltas
  '''
    try:
        delta_dict = dict(neuron_deltas)
    except TypeError:
        return False
    else:
        valid = True
        # verify the values are in appropriate range...
        for k, v in neuron_dict.items():
            valid = valid and is_delta(v)
        # ... and it has at least one delta
        valid = valid and len(delta_dict) > 1

        return valid

# Generators
# Helper
# def generate(num, generator, *config):
#  generated = []
#  for i in xrange(num):
#    generated.append(generator(*config))
#  return tuple(generated)


def many(num, x):
    xs = []
    for _ in xrange(num):
        xs.append(x)
    return xs


def repeatedly(num, fn):
    return map(lambda x: fn(), range(num))


def generate(generator, *arg_lists):
    if len(inspect.getargspec(generator).args) is 0:
        return many(arg_lists[0], generator())
    else:
        return map(lambda x: generator(*x), (zip(*arg_lists)))


def weight():
    return random.uniform(-1.0, 1.0)


def bias():
    return random.uniform(0.0, 1.0)


# Contracts
new_contract('signal', is_signal)
new_contract('delta', is_delta)
new_contract('weight', is_weight)
new_contract('bias', is_bias)
new_contract(
    'deltas',
    lambda ds: isinstance(
        ds,
        list) and len(ds) > 0 and all_true(
            ds,
        is_delta))
new_contract(
    'weights',
    lambda ws: (
        isinstance(
            ws,
            list) or isinstance(
                ws,
                tuple)) and len(ws) > 0 and all_true(
                    ws,
        is_weight))
new_contract(
    'inputs',
    lambda ns: isinstance(
        ns,
        list) and len(ns) > 0 and all_true(
            ns,
        is_signal))
new_contract(
    'outputs',
    lambda ns: isinstance(
        ns,
        list) and len(ns) > 0 and all_true(
            ns,
        is_signal))
new_contract(
    'learning_rate',
    lambda n: isinstance(
        n,
        int) or isinstance(
            n,
        float))
new_contract('neuron', is_neuron)


@contract
def neuron(num_weights):
    '''
    param weights: The list weights used to adjust output signals coming from upstream
    type weights: is_weights
    param bias: Bias for the neuron
    type bias: is_bias

    :param num_weights: Number of weights (1 per input neuron) this neuron needs
    :type num_weights: int,>0

    :return: A new neuron
    :rtype: neuron
  '''
    return {'bias': bias(),
            'weights': tuple(repeatedly(num_weights, weight))}


def weights(num):
    return make(num, weight)


def layer(prev_size, size):
    return tuple(generate(neuron, many(size, prev_size)))

# activation functions


def logistic_sigmoid(x):
    ''' Range is constrained between (0.0 and 1.0)
  Domain is any number '''
    return 1.0 / (1.0 + math.e ** -x)


5


def logistic_sigmoid_prime(x):
    ''' Where x is the output of the logistic_sigmoid function. '''
    return x * (1.0 - x)


default_activation_fns = [logistic_sigmoid, logistic_sigmoid_prime]


def neural_network(layer_sizes, activation_fns=default_activation_fns):
    return {'neurons': tuple(generate(layer,
                                      layer_sizes[:-1],
                                      layer_sizes[1:])),
            'fire': activation_fns[0],
            'fire_prime': activation_fns[1]}


def is_nn_signals(feedforward_data):
    ''' Verify that feedforward data are all valid signals. '''
    valid = True
    for layer in feedforward_data:
        for signal in layer:
            print valid
            valid = valid and is_signal(signal)
    return valid


def feedforward(nn, inputs):
    ''' Returns a datastructure containing signal information needed to train the network. '''
    nn_signals = []

    for layer in nn['neurons']:
        signals_in_layer = []

        for neuron in layer:
            input_signal = neuron['bias']  # + weight1 * input1 + ...
            for weight, _input in zip(neuron['weights'], inputs):
                input_signal += weight * _input

            output_signal = nn['fire'](input_signal)

            signals_in_layer.append({'signal_in': input_signal,
                                     'inputs': inputs,
                                     'signal_out': output_signal})

        # outputs of current layer will be used as inputs to the next one
        nn_signals.append(tuple(signals_in_layer))
        inputs = map(lambda signal: signal['signal_out'], nn_signals[-1])

    return tuple(nn_signals)

# what do I want backpropagation to return?
# conceptually, the changes to apply to weights
# in practice this would be a list of changes for each neuron, and a list
# of neurons per layer, and a list of layers comprising the nn. so as many
# values and in roughly the same shape as weights in the nn


def is_goals(xs):
    return all_true(is_signal_out, xs)


def is_learning_params(learning_params):
    if has_all_keys(
        k in learning_params for k in (
            'cost',
            'cost_prime',
            'learning_rate')):
        return True
    return False
    # return True if 'cost' in learning_params and 'cost_prime' in
    # learning_params else False


# derivative functions used in training
def signal_out_per_signal_in(fire_prime, signal_in):
    return fire_prime(signal_in)


def cost_per_signal_out(cost_prime, signal_out, goal):
    return cost_prime(signal_out, goal)


def signal_in_per_weight(signal_in_per_neuron):
    return signal_in_per_neuron


def signal_in_per_bias():
    return 1


def cost_per_weight(cost_per_signal_in, _input):
    return cost_per_signal_in * signal_in_per_weight(_input)

# cost functions


def cost(signal_out, goal):
    return 0.5 * (goal - signal_out) ** 2


def cost_prime(signal_out, goal):
    ''' Change in the cost function with respect to a change in the output. '''
    return (goal - signal_out) * -1


def backpropagation(nn, nn_signals, goals, learning_params):
    '''
    :param nn: The neural network to generate adjustments for.
    :type nn: is_neural_network

    :param nn_signals: The results of sending inputs with feedforward to a nn.
    :type nn_signals: is_nn_signals

    :param goals: The desired output for the nn_signals.
    :type goals: is_goals

    :param learning_params: Variables affecting backprop
    :type learning_params: is_learning_params


  '''

    outputs = map(lambda x: x['signal_out'], nn_signals[-1])
    errors = map(lambda pair: learning_params['cost'](*pair),
                 zip(outputs, goals))
    all_adj = []

    output_adj = []
    learning_rate = learning_params['learning_rate']

    cost_per_signal_in_D = []

    for output_neuron, neuron_signals, goal in zip(
            nn['neurons'][-1], nn_signals[-1], goals):
        # calculate the cost_per_signal_in aka 'delta'
        cost_per_signal_in = (
            cost_per_signal_out(
                learning_params['cost_prime'],
                neuron_signals['signal_out'],
                goal) *
            signal_out_per_signal_in(
                nn['fire_prime'],
                neuron_signals['signal_out']))

        cost_per_signal_in_D.append(cost_per_signal_in)

        # calculate output bias contribution to error
        cost_per_bias = cost_per_signal_in * signal_in_per_bias()
        bias_adj = -1.0 * learning_rate * cost_per_bias

        # calculate the costs per weight, using the cost_per_signal_in and
        # signal_in_per_weight
        weight_costs = map(
            functools.partial(
                cost_per_weight,
                cost_per_signal_in),
            neuron_signals['inputs'])
        debug("weight_costs: " + str(weight_costs), ERROR)
        # the -1.0 is because we want to reduce costs over time
        weight_adjs = [-1.0 * learning_rate * cost for cost in weight_costs]

        output_adj.append({'adj_bias': cost_per_bias,
                           'adj_weight': tuple(weight_adjs)})
    output_adj = tuple(output_adj)

    all_adj.append(output_adj)

    # calculate hidden weights contribution to error
    downstream_deltas = cost_per_signal_in_D
    for hidden_layer, downstream_layer, nn_signals in zip(
            nn['neurons'][-2::-1], nn['neurons'][::-1], nn_signals[-2::-1]):
        hidden_adj, downstream_deltas = hidden_adjs(
            hidden_layer, downstream_layer, downstream_deltas, nn_signals, nn['fire_prime'], learning_params)
        all_adj.append(tuple(hidden_adj))

    return {'adjustments': tuple(all_adj[::-1]), 'errors': errors}


def signal_in_D_per_signal_out(neuron, weight_pos):
    return neuron['weights'][weight_pos]


def mul(*xs):
    total = 1
    for x in xs:
        total *= x
    return total


def hidden_adjs(
        hidden_layer,
        downstream_layer,
        downstream_deltas,
        nn_signals_layer,
        fire_prime,
        learning_params):

    hidden_adjs = []
    hidden_deltas = []
    learning_rate = learning_params['learning_rate']

    # get the deltas for each hidden neuron
    for i, (hidden_neuron, hidden_signal) in enumerate(
            zip(hidden_layer, nn_signals_layer)):

        # get the ith weight for each downstream neuron
        ith_weights = [dl['weights'][i] for dl in downstream_layer]

        # get the delta for this neuron...
        #...get the sigma of cost_per_downstream_signal_out * downstream_signal_out_per_downstream_signal_in * downstream_signal_in_per_signal_out
        hidden_delta = 0
        for downstream_delta, weight in zip(downstream_deltas, ith_weights):
            hidden_delta += downstream_delta * weight

        #...get the signal_out_per_signal_in of the hidden neuron
        hidden_delta *= signal_out_per_signal_in(
            fire_prime, hidden_signal['signal_out'])
        hidden_deltas.append(hidden_delta)
        cost_per_signal_in = hidden_delta

        weight_costs = []
        for _input in hidden_signal['inputs']:
            # get weight cost
            weight_costs.append(
                cost_per_weight(
                    cost_per_signal_in,
                    signal_in_per_weight(_input)))

        # the -1.0 is because we want to reduce costs over time
        weight_adjs = [-1.0 * learning_rate * cost for cost in weight_costs]

        # calculate hidden bias contribution to error
        cost_per_bias = cost_per_signal_in * signal_in_per_bias()

        # get bias adjustment
        bias_adj = -1.0 * learning_rate * cost_per_bias

        hidden_adj = {'adj_weight': tuple(weight_adjs), 'adj_bias': bias_adj}
        hidden_adjs.append(hidden_adj)

    return (hidden_adjs, hidden_deltas)


def apply_adjustments(nn, nn_adjustments, apply_bias=False):
    new_nn = []
    for layer, layer_adjustments in zip(nn['neurons'], nn_adjustments):
        new_layer = []
        for neuron, neuron_adjustments in zip(layer, layer_adjustments):
            new_neuron = {'weights': [],
                          'bias': neuron['bias']}
            if apply_bias:
                new_neuron['bias'] = new_neuron['bias'] + \
                    neuron_adjustments['adj_bias']
            for weight, weight_adj in zip(
                    neuron['weights'], neuron_adjustments['adj_weight']):
                new_neuron['weights'].append(weight + weight_adj)
            new_neuron['weights'] = tuple(new_neuron['weights'])
            new_layer.append(new_neuron)
        new_nn.append(tuple(new_layer))
    return {'neurons': tuple(new_nn),
            'fire': nn['fire'],
            'fire_prime': nn['fire_prime']}


def train_once(nn, learning_params, inputs, goals):
    '''
  Returns a new nn trained on the inputs to move towards the goals
  '''
    nn_signals = feedforward(nn, inputs)
    adjustments_and_errors = backpropagation(
        nn, nn_signals, goals, learning_params)
    adjustments = adjustments_and_errors['adjustments']
    errors = adjustments_and_errors['errors']
    return (apply_adjustments(nn, adjustments), sum(errors))


def train_set(nn, learning_params, training_data):
    error = 1
    count = 0
    while error > training_data['max_error'] and count < training_data['max_tries']:
        for inputs, goals in zip(
                training_data['inputs'], training_data['goals']):
            nn, error = train_once(nn, learning_params, inputs, goals)
        count += 1
    return (nn, count < training_data['max_tries'], count)
