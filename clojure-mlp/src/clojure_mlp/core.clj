(ns clojure-mlp.core
  "resources for more info
theclevermachine.wordpress.com
http://mattmazur.com/2015/03/17/a-step-by-step-backpropagation-example/comment-page-2/ "
  (:gen-class)
  (:require
   [clojure.spec.alpha :as s]
   [clojure.core.matrix :as mx]))

;;;;; spec
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(s/def :neuron/type #(or (= % :neuron) (= % ::neuron)))
(s/def :layer/type #(or (= % :layer) (= % ::layer)))
(s/def :nn/type #(or (= % :nn) (= % ::nn)))
(s/def ::weight number?)
(s/def ::weights (s/coll-of ::weight))
(s/def ::bias number?)
(s/def ::activation (s/coll-of fn?))
(s/def ::neuron (s/keys :req-un [:neuron/type ::weights ::bias]))
(s/def ::neurons (s/coll-of ::neuron))
(s/def ::layer (s/keys :req-un [:layer/type ::neurons]))
(s/def ::layers (s/coll-of ::layer))
(s/def ::nn (s/keys :req-un [:nn/type ::layers]))

(s/def ::alpha number?)
(s/def ::inputs (s/coll-of number?)) ;; in principle the exact bounds on this will depend on, but not be equivalent to, the bounds on outputs
(s/def ::delta number?)
(s/def ::deltas (s/coll-of ::delta))
(s/def ::outputs (s/coll-of number?)) ;; any bounds that might exist on outputs will depend on the activation function, which should be flexible
(s/def ::err (s/coll-of number?))
(s/def :neuron/adjustments-in (s/keys :req-un [::neuron ::inputs ::delta ::alpha])) ;; TODO shouldn't this be deltas (plural)
(s/def :neuron/adjustments-out ::neuron)
(s/def :layer/adjustments-in (s/keys :req-un [::layer ::inputs ::deltas ::alpha]))
(s/def :layer/adjustments-out ::layer)
(s/def :nn/adjustments-in (s/keys :req-un [::nn ::outputs ::deltas ::inputs ::alpha]))
(s/def :nn/adjustments-out ::nn)

;;;;; constants
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(def log-file "log")

;;;;; functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; activation functions, these are used to bound a neurons output when fired
(defn log
  [x fn-name]
  (or (spit log-file (str fn-name ": " x "\n") :append true)
      true))

(defn logistic-sigmoid
  "Bound the output between 0 and 1"
  [x]
  (/ 1 (+ 1 (Math/exp (- x)))))

(defn logistic-sigmoid-prime
  "Used in backpropagation.

  Specifically used to determine how the output signal of a neuron varies with respect to its net input.
  x should always be the net inputs to the neuron (e.g. (w1 * inp1) + (w2 *inp2) ... (wN +inpN) + bias)"
  [x]
  (* x (- 1 x)))

;; cost functions
(defn err
  "Returns the error of the result and the target."
  [res target]
  {:post [(number? %)]}
  (Math/pow (- target res) 2))

(defn total-err
  "Returns the total error given a list of results and list of targets."
  [res target]
  {:pre [(s/valid? (s/coll-of number?) res)
         (s/valid? (s/coll-of number?) target)
         (= (count res) (count target))]
   :post [(number? %)]}
  (apply + (map err res target)))

(defn neuron
  [num-inputs]
  {:pre  [(int? num-inputs)]
   :post [(s/valid? ::neuron %)]}
  {:weights (repeatedly num-inputs #(dec (rand 2)))
   :bias    (dec (rand 2))
   :type    ::neuron})

(defn layer
  [prev-layer-size curr-layer-size]
  {:pre  [(int? prev-layer-size) (int? curr-layer-size)]
   :post [(s/valid? ::layer %)]}
  {:neurons (repeatedly curr-layer-size #(neuron prev-layer-size))
   :type ::layer})

(defn nn
  [layer-sizes]
  {:pre [(every? int? layer-sizes)]
   :post [(s/valid? ::nn %)]}
  {:layers (map layer layer-sizes (next layer-sizes))
   :type ::nn})


;; fire functions
(defmulti fire :type)

(defmethod fire ::neuron
  [{:keys [weights bias inputs activation]}]
  (->> (mx/mul (conj weights bias)
               (conj inputs 1))
       (reduce +)
       activation))

(defmethod fire :neuron
  [neuron]
  (fire (assoc neuron :type ::neuron)))

(defmethod fire ::layer
  [{:keys [neurons inputs activation]}]
  (mapv (comp fire
              #(assoc % :inputs inputs :activation activation))
        neurons))

(defmethod fire :layer
  [layer]
  (fire (assoc layer :type ::layer)))

(defmethod fire ::nn
  [{:keys [layers inputs activation]}]
  "Returns a vector of hashmaps containing firing information about the output layer.
E.g. the first item in the vector is a hashmap containing the inputs to the layer, the activation fn used to fire and the output of calling fire on the neurons"
  (loop [inputs  inputs
         layers   layers
         outputs []]
    (if (not-empty layers)
      (recur nil
             (next layers)
             (conj outputs (fire (assoc (first layers)
                                         :inputs (or inputs (last outputs))
                                         :activation activation))))
      outputs)))

(defmethod fire :nn
  [nn]
  (fire (assoc nn :type ::nn)))

;; delta functions
(defn delta-out
  [target actual]
  {:pre []
   :post [(number? %)]}
  (* -1 ;; dE/dOut
     (- target actual) ;; dE/dOut
     actual ;; dOut/dNet
     (- 1 actual) ;; dOut/dNet
     ))

(defn delta-hidden
  "list of deltas from output neurons
  list of weights to output neurons from this hidden neuron
  output of hidden neuron"
  [delta-out weight-out output]
  {:pre [(float? output)
         (reduce #(and %1 (float? %2)) true  weight-out)
         (reduce #(and %1 (float? %2)) true  delta-out)]
   :post [(float? %)]}
  (* (reduce #(+ %1 (apply * %2)) 0 (map vector delta-out weight-out))
     output
     (- 1 output)))

(defn delta-output-layer
  [nn target output]
  (mapv delta-out target (last output)))

(defn delta-hidden-layer
  "Expects output param is doesn't contain output layer outputs.
  Returns delta obj a list of lists of deltas
  
  nn-layers is the :layers value from a nn
  above-delta is...
  output is... a vector of vectors containing neuron outputs. each value is a neuron output, each inner vector is a layer of outputs, the outer vector is the total outputs of a nn
  delta-acc is... the recursively accumulated output of the function. a list of lists of deltas "
  [nn-layers above-delta output delta-acc]
  (let [layer-deltas (mapv #(delta-hidden above-delta %1 %2)
                           (partition (count (:neurons (last nn-layers)))
                                      (apply interleave (map :weights (:neurons (last nn-layers)))))
                           (last output))]
    (if (next nn-layers)
      ;; recur dropping off a list from output and nn-layers, conj onto acc the previous layers delta and the current layers deltas
      (delta-hidden-layer (butlast nn-layers)
                          layer-deltas
                          (butlast output)
                          (conj (if (empty? delta-acc)
                                  (conj delta-acc above-delta)
                                  delta-acc)
                                layer-deltas))
      (into [] (reverse delta-acc)))))

(defn delta
  "Given a nn and its outputs, return an [nn, output, deltas, total-err] datastructure"
  [[nn output] target]
  [nn
   output
   (delta-hidden-layer (:layers nn)
                       (delta-output-layer nn target output)
                       (butlast output)
                       [])
   (total-err (last output) target)])

;; adjustment functions
(defn weight-err
  [delta inp learning-rate]
  {:post [(number? %)]}
  (* delta inp (- learning-rate)))

(defn new-weight
  "Returns the new weight given a weight, delta, input, and learning rate."
  [w d i alpha]
  (+ w (weight-err d i alpha)))

(defn adj-weights-neuron
  "Updates the weights and bias in a neuron."
  [n inputs delta alpha]
  {:pre [(s/valid? ::neuron n)]
   :post [(s/valid? :neuron/adjustments-out %)]}
  (-> n
      (update :weights (fn [weight-list]
                   (map #(new-weight %1 delta %2 alpha)
                        weight-list
                        inputs)))
      (update :bias #(new-weight % delta 1 alpha))))

(defn adj-weights-layer
  "Returns an updated layer."
  [layer inputs deltas alpha]
  {:post [(s/valid? :layer/adjustments-out %)]}
  {:type ::layer
   :neurons (mapv #(adj-weights-neuron %1 inputs %2 alpha) (:neurons layer) deltas)})

(defn inputs-by-layer
  [outputs inputs]
  (conj (butlast outputs) inputs))

(defn adj-weights-nn
  "Returns an updated nn"
  [[nn outputs deltas err] inputs alpha]
  {:post [(s/valid? :nn/adjustments-out %)]}
  {:type ::nn
   :layers (mapv adj-weights-layer
                 (:layers nn)
                 (inputs-by-layer outputs inputs)
                 deltas
                 (repeatedly (constantly alpha)))})

;; training functions
(defn train-once
  "Trains the nn once. Returns a vector containing the updated nn and the error e.g. [new-nn error]."
  [nn [inputs target] {:keys [alpha activation] :as train-ds}]
  {:pre []
   :post []}
  (let [outputs                      (fire (assoc nn :inputs inputs :activation activation))
        [nn output deltas total-err] (delta [nn outputs] target)
        error (first (mapv err (last outputs) target))
        adj-res (adj-weights-nn [nn output deltas total-err] inputs alpha)]
    [adj-res total-err]))

(defn train-set
  "Trains the nn for the number of times specfied in the train-ds"
  [nn [inp target err] train-ds]
  (if (< 0 (:sets train-ds))
    (let [result (train-once nn [inp target] train-ds)]
      (train-set (first result)
                 [inp target (second result)]
                 (update train-ds :sets dec)))
    [nn err]))

(defn train
  "Trains the nn for the number of times specfied in the train-ds. Returns when the goal error has been reached for each training set in a single pass or when the max number of tries has been reached. Returns a vector containing the trained nn and a success/failure message e.g. [new-nn \"Success\"]"
  [nn training-data train-ds]
  ;;assume don't cut off early
  (loop [i 0
         nn nn]
    (let [train-res (reduce (fn [[nn err-vec] [input target]]
                              (let [res (train-set nn [input target] train-ds)]
                                (assoc res 1 (conj err-vec (second res)))))
                            [nn []]
                            training-data)]

      ;;(clojure.pprint/pprint (str "train-res: \n" ))
      ;;(clojure.pprint/pprint train-res)
      (if (->> train-res
               second ;;get the error vector
               (apply max) ;;get max error
               (> (:goal train-ds)))
        ;;done, success
        [train-res "Success"]
        (if (= i (:loops train-ds))
          ;;done, failed
          [train-res "Failure"]
          ;;not done yet
          (recur (inc i) (first train-res)))))))

(defn -main
  "Runs a preconfigured set of training sessions for xor boolean function printing out the results."
  [& args]
  (println
   (let [trials 100
         xor-td {[0 0] [0]
                 [0 1] [1]
                 [1 0] [1]
                 [1 1] [0]}
         target-err 0.01
         train-ds {:goal target-err
                   :sets 100
                   :loops 20
                   :alpha 0.75
                   :activation logistic-sigmoid}]
     (loop [i 0
            successes 0]
       (if (< i trials)
           (recur (inc i)
                  (if (= (second
                          (train (nn [2 4 1])
                                 xor-td
                                 train-ds))
                         "Success")
                    (inc successes)
                    successes))
           (str "Trained xor successfully " successes " out of " trials " times with an acceptable error of " target-err "."))))))
