(ns clojure-mlp.core-test
  (:require [clojure.test :refer :all]
            [clojure-mlp.core :refer :all]))

(deftest -test
  (testing "Test *** function."
    (is true)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; HELPER FUNCTIONS ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defmacro setq [& forms]
  `(do ~@(for [[name body] (partition 2 forms)]
           `(def ~name ~body))))

(defn approx
  "Checks if two numbers approximately equal each other."
  [n1 n2 tolerance]
  (<= (Math/abs (- n1 n2)) tolerance))

(defn approx-li
  "Checks if a list of numbers approximately equals a second list of numbers by comparing the
first of list1 to the first of list2 and the second of list1 to second of list2 etc. Return a single boolean value for equality."
  [lis1 lis2 tolerance]
  ;;todo implement this with loop recur or just using recursion (don't know why it isn't already)
  (reduce #(and %1 %2)
          (map #(approx (first %) (second %) tolerance)
               (partition 2 (interleave lis1 lis2)))))

(defn partial-err-out
  [target actual]
  ;;longer form is (+ (* 2 0.5 (Math/pow (- target actual) (- 2 1) -1) 0)
  (- actual target))

(defn ever-true
  "Returns true if a list contains any true values."
  [lis]
  (reduce #(if (= true %2) true %1) lis))

(declare xor-td)
(declare or-td)
(declare and-td)
(declare nand-td)
(defn train-xor
  []
  (let [training-data xor-td
        target-err 0.01
        loops 20
        sets 100
        alpha 0.75
        train-ds {:goal target-err
                  :sets sets
                  :loops loops
                  :alpha alpha
                  :activation logistic-sigmoid}
        _nn (nn [2 4 1])
        result (second (train _nn training-data train-ds))]
    (= result "Success")))

(defn train-or
  []
  (let [training-data or-td
        target-err 0.01
        loops 20
        sets 100
        alpha 1.00
        train-ds {:goal target-err
                  :sets sets
                  :loops loops
                  :alpha alpha
                  :activation logistic-sigmoid}
        _nn (nn [2 4 1])
        result (second (train _nn training-data train-ds))]
    (= result "Success")))

(defn train-and
  []
  (let [training-data and-td
        target-err 0.01
        loops 20
        sets 100
        alpha 0.75
        train-ds {:goal target-err
                  :sets sets
                  :loops loops
                  :alpha alpha
                  :activation logistic-sigmoid}
        _nn (nn [2 4 1])
        result (second (train _nn training-data train-ds))]
    (= result "Success")))

(defn train-nand
  []
  (let [training-data nand-td
        target-err 0.01
        loops 20
        sets 100
        alpha 0.75
        train-ds {:goal target-err
                  :sets sets
                  :loops loops
                  :alpha alpha
                  :activation logistic-sigmoid}
        _nn (nn [2 4 1])
        result (second (train _nn training-data train-ds))]
    (= result "Success")))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; EXAMPLE DATA ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;example data from http://mattmazur.com/2015/03/17/a-step-by-step-backpropagation-example/comment-page-2/
(setq

      ;;net refers to the total net input to a neuron
      ;;actual refers to a neurons output (the net squashed by an activation function)

      ;;sigfigs
      tolerance 0.000001
      eq-ish #(approx %1 %2 tolerance)
      eq-ish-li #(approx-li %1 %2 tolerance)

      ;;learning rate
      alpha 0.5

      ;;inputs
      i1 0.05
      i2 0.10
      ;;initial weights
      w1 0.15
      w2 0.20
      w3 0.25
      w4 0.30
      w5 0.40
      w6 0.45
      w7 0.50
      w8 0.55
      ;;biases (threshold)
      b1 0.35 ;;used for h1 and h2
      b2 0.60 ;;used for o1 and o2


      ;;targets and actual
      t-o1 0.01
      a-o1 0.75136507
      n-o1 1.105905967
      t-o2 0.99
      a-o2 0.772928465
      n-o2 nil ;;not defined, not easily reversable
      n-h1 0.3775 ;;net inputs of hidden neuron, h1
      n-h2 (+ (* w3 i1) (* w4 i2) b1) ;;net inputs of hidden neuron, h2
      a-h1 0.5932699992 ;;output of hidden neuron, h1
      a-h2 0.596884378 ;;output of hidden neuron, h2

      ;;errors
      e-o1 0.274811083
      e-o2 0.023560026
      e-total (apply + (map err [a-o1 a-o2] [t-o1 t-o2])) ;;(+ e-o1 e-o2)

      ;;backprop
      et-eo1 (+ (* 2 0.5 (- t-o1 a-o1) -1) 0) ;;partial error of output1
      et-eo2 (+ (* 2 0.5 (- t-o2 a-o2) -1) 0) ;;partial error of output2
      do-o1_dn-o1 0.186815602 ;;delta output1 over delta net output 1

      ;;adjusted weight (these were given, work backwards for weight deltas and delta errors
      w5-prime 0.35891648 ;;updated weight after applying learning rate and calculating error adjustment
      w6-prime 0.408666186
      w7-prime 0.511301270
      w8-prime 0.561370121
      w1-prime 0.149780716
      w2-prime 0.19956143
      w3-prime 0.24975114
      w4-prime 0.29950229

      ;;weight delta error
      wd5 0.082167041 ;;error adjustment to weight, before including error rate
      wd6 (/ (- w6-prime w6) (- alpha))
      wd7 (/ (- w7-prime w7) (- alpha))
      wd8 (/ (- w8-prime w8) (- alpha))
      wd1 0.000438568
      wd2 (/ (- w2-prime w2) (- alpha))
      wd3 (/ (- w3-prime w3) (- alpha))
      wd4 (/ (- w4-prime w4) (- alpha))

      ;;delta errors (not shown in example, must be calculated)
      d5 (/ wd5 a-h1) ;;result of the delta function for the given weight
      d6 (/ wd6 a-h2)
      d7 (/ wd7 a-h1)
      d8 (/ wd8 a-h2)
      d1 (/ wd1 i1) ;;(* 0.036350306 0.241300709) ;;(/ w1 i1)
      d2 (/ wd2 i2)
      d3 (/ wd3 i1)
      d4 (/ wd4 i2)
      do1 (* (- a-o1 t-o1) a-o1 (- 1 a-o1))
      do2 (* (- a-o2 t-o2) a-o2 (- 1 a-o2))


      ;;output ds
      ds-out [[a-h1 a-h2]
              [a-o1 a-o2]]
      input-lis [i1 i2]
      target-lis [t-o1 t-o2]




      n1 {:type :neuron :weights [w1 w2] :bias b1 :activation [logistic-sigmoid logistic-sigmoid-prime]}
      n2 {:type :neuron :weights [w3 w4] :bias b1 :activation [logistic-sigmoid logistic-sigmoid-prime]}
      n3 {:type :neuron :weights [w5 w6] :bias b2 :activation [logistic-sigmoid logistic-sigmoid-prime]}
      n4 {:type :neuron :weights [w7 w8] :bias b2 :activation [logistic-sigmoid logistic-sigmoid-prime]}

      lay1 {:type :layer :neurons [n1 n2]}
      lay2 {:type :layer :neurons [n3 n4]}

      _nn {:type :nn :layers [lay1 lay2]}

      nn-layers (:layers _nn)
      above-delta (delta-output-layer [] [t-o1 t-o2] [[a-o1 a-o2]])
      output  [[a-h1 a-h2] [a-o1 a-o2]]
      delta-acc []

      data-set1 [[0 1] [1]]
      data-set2 [[1 0] [1]]
      data-set3 [[1 1] [0]]
      data-set4 [[0 0] [0]]

      data-set [[i1 i2] [t-o1 t-o2]]
      ex-training-data [data-set]
      xor-training-data [data-set1 data-set2 data-set3 data-set4]
      target-err 0.001
      loops 100
      sets 10
      alpha alpha
      train-ds {:goal target-err
                :sets sets
                :loops loops
                :alpha alpha}
      ;;datastructures for simple boolean logic functions
      or-td {[0 0] [0]
             [0 1] [1]
             [1 0] [1]
             [1 1] [1]}
      xor-td {[0 0] [0]
              [0 1] [1]
              [1 0] [1]
              [1 1] [0]}
      and-td {[0 0] [0]
              [0 1] [0]
              [1 0] [0]
              [1 1] [1]}
      nand-td {[0 0] [1]
               [0 1] [1]
               [1 0] [1]
               [1 1] [0]})


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; TESTS ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(deftest partial-err-out-test
  (testing "Test partial-err-out function."
    (is (and (= et-eo1 (partial-err-out t-o1 a-o1))
             (= et-eo2 (partial-err-out t-o2 a-o2))))))

(deftest logistic-sigmoid-prime-test
  (testing "Test logistic-sigmoid-prime function."
    (is (eq-ish do-o1_dn-o1 (logistic-sigmoid-prime a-o1)))))

;;check neuron inputs
(deftest net-inputs-test
  (testing "Test net inputs calculation."
    (is (and (eq-ish n-h1 (+ (* w1 i1) (* w2 i2) b1))
             (eq-ish n-o1 (+ (* w5 a-h1) (* w6 a-h2) b2))))))

;;check neuron outputs, logistic-sigmoid is activation function
(deftest logistic-sigmoid-test
  (testing "Test logistic-sigmoid function."
    (is (and (eq-ish a-h1 (logistic-sigmoid n-h1))
             (eq-ish a-h2 (logistic-sigmoid n-h2))
             (eq-ish a-o1 (logistic-sigmoid n-o1))))))

;;check weight delta errors
(deftest weight-delta-test
  (testing "Test weight delta calculation."
    (is (eq-ish wd1 (/ (- w1-prime w1) (- alpha))))))

;;check delta errors
(deftest delta-test
  (testing "Test delta calculation."
    (is (eq-ish d1 (/ wd1 i1)))))

;;check delta-out function
;;note since d5 and d6 have same calculation deltas are unique per neuron not per weight
(deftest delta-out-test
  (testing "Test delta out function."
    (is (and (eq-ish d5 (delta-out t-o1 a-o1))
             (eq-ish d6 (delta-out t-o1 a-o1))
             (eq-ish d7 (delta-out t-o2 a-o2))
             (eq-ish d8 (delta-out t-o2 a-o2))
             (eq-ish d5 d6)
             (eq-ish d7 d8)))))

;;calculate delta dN, based on hN's weights and deltas
;;note since d1 and d2 have same calculation deltas are unique per neuron not per weight
(deftest delta-hidden-test
  (testing "Test delta-hidden function."
    (is (and (eq-ish d1 (delta-hidden [d5 d7] [w5 w7] a-h1))
             (eq-ish d2 (delta-hidden [d5 d7] [w5 w7] a-h1))
             (eq-ish d3 (delta-hidden [d6 d8] [w6 w8] a-h2))
             (eq-ish d4 (delta-hidden [d6 d8] [w6 w8] a-h2))))))

;;check delta-hidden-layer function
(deftest delta-hidden-layer-test
  (testing "Test delta-hidden-layer function."
    (is (reduce #(and %1 %2) (map #(eq-ish (first %) (second %))
                                  (partition 2
                                             (flatten (map #(partition 2 (interleave %1 %2))
                                                           [[d1 d3] [d5 d7]]
                                                           (delta-hidden-layer (:layers _nn) above-delta (butlast output) [])))))))))

;;compare weight adjustments
(deftest weight-delta-test
  (testing "Test weight-delta calculation."
    (is (eq-ish wd1 (* (delta-hidden [d5 d7] [w5 w7] a-h1) i1)))))

       ;;compare updated weights
(deftest weight-change-test
  (testing "Test weight change calculation."
    (is (eq-ish w1-prime (+ w1 (weight-err (delta-hidden [d5 d7] [w5 w7] a-h1) i1 alpha))))))


;;check delta function
(deftest delta-test
  (testing "Test delta function."
    (is (let [_nn       (assoc _nn :inputs [i1 i2] :activation logistic-sigmoid) 
              ;;nn-ds (nth (delta (fire _nn #_[_nn [i1 i2]]) [t-o1 t-o2]) 2)
              output-ds (nth (delta [_nn (fire _nn #_[_nn [i1 i2]])] #_(fire _nn #_[_nn [i1 i2]]) [t-o1 t-o2]) 2)
              deltas-ds (nth (delta [_nn (fire _nn #_[_nn [i1 i2]])] #_(fire _nn #_[_nn [i1 i2]]) [t-o1 t-o2]) 2)
              total-err (nth (delta [_nn (fire _nn #_[_nn [i1 i2]])]  [t-o1 t-o2]) 3)]
        (and
            ;;check hidden layer deltas
            (eq-ish-li (first deltas-ds)
                       [d1 d3])
            ;;check output layer deltas
            (eq-ish-li (second deltas-ds)
                       [d5 d7])
            ;;check total error
            (eq-ish total-err
                    e-total))))))

(deftest adj-weights-test
  (testing "Test adj-weights function."
    (is (eq-ish-li (:weights (adj-weights-neuron n1 [i1 i2] d1 alpha))
                   [w1-prime w2-prime]))))

(deftest train-boolean
  (testing "Test training XOR, OR, AND, and NAND boolean logic functions."
    (is (ever-true (take 5 (repeatedly train-xor))))
    (is (ever-true (take 5 (repeatedly train-or)))) 
    (is (ever-true (take 5 (repeatedly train-and))))
    (is (ever-true (take 5 (repeatedly train-nand))))))
